﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobPortal.API.Model
{
    [Table("Job")]
    public class Job
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Job Title Required")]
        [StringLength(100)]
        public string JobTitle { get; set; }

        [Required(ErrorMessage = "Job Description is Required")]
        [StringLength(1000)]
        public string JobDescription { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
