﻿namespace JobPortal.API.Model
{
    public class Mail
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public int UserId { get; set; }
        public string JobTitle { get; set; }
    }
}
