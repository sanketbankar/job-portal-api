﻿using System.ComponentModel.DataAnnotations;

namespace JobPortal.API.Model.DTOs
{
    public class LoginModelDTO
    {
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }

    }
}
