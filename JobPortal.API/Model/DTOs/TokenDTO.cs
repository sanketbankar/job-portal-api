﻿using System;

namespace JobPortal.API.Model.DTOs
{
    public class TokenDTO
    {
        public string Token { get; set; }
        public DateTime ExpireTime { get; set; }
    }
}
