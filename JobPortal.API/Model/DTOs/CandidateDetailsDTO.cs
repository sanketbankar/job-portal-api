﻿

namespace JobPortal.API.Model.DTOs
{
    public class CandidateDetailsDTO : JobApplicant
    {
        public int UserId { get; set; }
        public int JobId { get; set; }
        public string ApplicantName { get; set; }
        public string JobTitle { get; set; }
        public int PostedBy { get; set; }
    }
}
