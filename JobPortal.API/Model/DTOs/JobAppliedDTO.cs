﻿
namespace JobPortal.API.Model.DTOs
{
    public class JobAppliedDTO : Job
    {
        public int JobId { get; set; }
        public int UserId { get; set; }
        public string JobTitle { get; set; }
    }
}
