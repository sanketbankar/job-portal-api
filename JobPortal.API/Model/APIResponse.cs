﻿namespace JobPortal.API.Model
{
    public class APIResponse
    {
        public int Code { get; set; } = 200;
        public string Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
