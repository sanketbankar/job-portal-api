﻿using System.ComponentModel.DataAnnotations;

namespace JobPortal.API.Model
{
    public class ForgotPassword
    {
        [Required, EmailAddress, Display(Name = "Enter Registered EmailId")]
        public string EmailId { get; set; }
    }
}
