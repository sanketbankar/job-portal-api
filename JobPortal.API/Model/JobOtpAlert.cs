﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobPortal.API.Model
{
    [Table("JobOtpAlert")]
    public class JobOtpAlert
    {
        public int Id { get; set; }
        public int Otp { get; set; }
        public int UserId { get; set; }
        public DateTime ExpireTime { get; set; }
    }
}
