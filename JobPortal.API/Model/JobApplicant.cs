﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobPortal.API.Model
{
    [Table("JobApplicant")]
    public class JobApplicant
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "JobId is required")]
        public int JobId { get; set; }
        public int AppliedBy { get; set; }
        public DateTime AppliedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
