﻿using JobPortal.API.Model;
using Microsoft.EntityFrameworkCore;

namespace JobPortal.API.Database
{
    public class JobPortalContext : DbContext
    {
        public JobPortalContext(DbContextOptions options) : base(options)
        {
           
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<JobApplicant> JobApplicants { get; set; }
        public DbSet<JobOtpAlert> JobOtpAlerts { get; set; }
    }
}
