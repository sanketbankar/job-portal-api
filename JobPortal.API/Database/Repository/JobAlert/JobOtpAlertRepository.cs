﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;

namespace JobPortal.API.Database.Repository.JobAlert
{
    public class JobOtpAlertRepository : Repository<JobOtpAlert> , IJobOtpAlertRepository
    {
        public JobOtpAlertRepository(JobPortalContext context) : base(context)
        {

        }
    }
}
