﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;

namespace JobPortal.API.Database.Repository.JobAlert
{
    public interface IJobOtpAlertRepository : IRepository<JobOtpAlert>
    {
    }
}
