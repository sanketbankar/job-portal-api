﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository.Users
{
    public interface IJobApplicantRepository : IRepository<JobApplicant>
    {
        Task<IEnumerable<RecruiterJobsDTO>> GetCandidateAppliedToRecruitersJob(int userId, PagingFilter pagingFilter);
        Task<IEnumerable<CandidateDetailsDTO>> GetAllCandidatesJobsApplied(PagingFilter pagingFilter);
        Task<IEnumerable<CandidateDetailsDTO>> GetCandidatesAllJobsApplied(int userId, PagingFilter pagingFilter);
    }
}
