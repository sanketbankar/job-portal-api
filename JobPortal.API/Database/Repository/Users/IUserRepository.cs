﻿

using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<User>> GetAdmin();
        Task<IEnumerable<User>> GetCandidates();
        Task<IEnumerable<User>> GetRecruiters();
        Task<IEnumerable<User>> GetAllUsers(PagingFilter pagingFilter);
        Task<IEnumerable<User>> GetUser();
    }
}
