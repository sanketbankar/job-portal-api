﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository.Users
{
    public class JobApplicantRepository : Repository<JobApplicant>, IJobApplicantRepository
    {
        public JobApplicantRepository(JobPortalContext context) : base(context)
        {

        }
        public async Task<IEnumerable<RecruiterJobsDTO>> GetCandidateAppliedToRecruitersJob(int userId, PagingFilter pagingFilter)
        {
            var applied = await (from ja in _context.JobApplicants
                                 join u in _context.Users on ja.AppliedBy equals u.Id
                                 join j in _context.Jobs on ja.JobId equals j.Id
                                 join us in _context.Users on j.CreatedBy equals us.Id
                                 select new RecruiterJobsDTO
                                 {
                                     ApplicantName = u.Name,
                                     JobId = ja.JobId,
                                     JobTitle = j.JobTitle,
                                     RecruiterId = j.CreatedBy,
                                     RecruiterName = us.Name,
                                     JobDescription = j.JobDescription,
                                     CreatedBy = j.CreatedBy,
                                     CreatedDate = j.CreatedDate,
                                     IsActive = u.IsActive
                                 }).Where(x => x.RecruiterId == userId).Skip((pagingFilter.PageNumber -1) *pagingFilter.PageSize)
                                   .Take(pagingFilter.PageSize)
                                   .OrderByDescending(x => x.JobId)
                                   .ToListAsync();
            return applied;
        }
        public async Task<IEnumerable<CandidateDetailsDTO>> GetAllCandidatesJobsApplied(PagingFilter pagingFilter)
        {
            var application = await (from a in _context.JobApplicants
                                     join u in _context.Users on a.AppliedBy equals u.Id
                                     join j in _context.Jobs on a.JobId equals j.Id
                                     select new CandidateDetailsDTO
                                     {
                                         UserId = u.Id,
                                         JobId = a.JobId,
                                         JobTitle = j.JobTitle,
                                         ApplicantName = u.Name,
                                         PostedBy = j.CreatedBy,
                                         AppliedBy = u.Id,
                                         AppliedDate = a.AppliedDate,
                                         IsActive = u.IsActive

                                     }).Skip((pagingFilter.PageNumber -1) * pagingFilter.PageSize)
                                       .Take(pagingFilter.PageSize)
                                       .OrderByDescending(x => x.JobId)
                                       .ToListAsync();
            return application;
        }
        public async Task<IEnumerable<CandidateDetailsDTO>> GetCandidatesAllJobsApplied(int userId, PagingFilter pagingFilter)
        {
            var applied = await (from a in _context.JobApplicants
                                 join u in _context.Users on a.AppliedBy equals u.Id
                                 join j in _context.Jobs on a.JobId equals j.Id
                                 select new CandidateDetailsDTO
                                 {
                                     UserId = u.Id,
                                     JobId = a.JobId,
                                     JobTitle = j.JobTitle,
                                     ApplicantName = u.Name,
                                     PostedBy = j.CreatedBy,
                                     AppliedBy = u.Id,
                                     AppliedDate = a.AppliedDate,
                                     IsActive = u.IsActive

                                 }).Where(x => x.AppliedBy == userId)
                                   .Skip((pagingFilter.PageNumber - 1)* pagingFilter.PageSize)
                                   .Take(pagingFilter.PageSize)
                                   .OrderByDescending(x => x.JobId)
                                   .ToListAsync();
            return applied;
        }
    }
}
