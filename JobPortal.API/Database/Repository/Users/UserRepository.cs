﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(JobPortalContext jobPortalcontext) : base(jobPortalcontext)
        {

        }
        public async Task<IEnumerable<User>> GetAllUsers(PagingFilter pagingFilter)
        {
            var users = await (from u in _context.Users
                               select u).Skip((pagingFilter.PageNumber - 1) * pagingFilter.PageSize)
                                   .Take(pagingFilter.PageSize)
                                   .ToListAsync();
            return users;
        }
        public async Task<IEnumerable<User>> GetUser()
        {
            var user = await (from u in _context.Users
                              select u).ToListAsync();
            return user;
        }
        public async Task<IEnumerable<User>> GetCandidates()
        {
            var candidates = await (from u in _context.Users
                                   where u.RoleId == 1
                                   select u).ToListAsync();
            return candidates;
        }

        public async Task<IEnumerable<User>> GetRecruiters()
        {
            var recruiter = await (from u in _context.Users
                                    where u.RoleId == 2
                                    select u).ToListAsync();
            return recruiter;
        }

        public async Task<IEnumerable<User>> GetAdmin()
        {
            var admin = await (from u in _context.Users
                                    where u.RoleId == 3
                                    select u).ToListAsync();
            return admin;
        }

    }
}
