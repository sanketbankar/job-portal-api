﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository.Jobs
{
    public class JobRepository : Repository<Job>, IJobRepository
    {
        public JobRepository(JobPortalContext context) : base(context)
        {
           
        }
        public async Task<IEnumerable<Job>> GetJobs(PagingFilter pagingFilter)
        {
            var job = await (from j in _context.Jobs
                             select j).Skip((pagingFilter.PageNumber - 1) * pagingFilter.PageSize)
                                      .Take(pagingFilter.PageSize)
                                      .OrderByDescending(x => x.CreatedDate)
                                      .ToListAsync();
            return job;
        }
        public async Task<IEnumerable<Job>> GetRecruiterPostedJobs(int userId, PagingFilter pagingFilter)
        {
            var recruiterJobs = await (from j in _context.Jobs
                                       where j.CreatedBy == userId
                                       select j).Skip((pagingFilter.PageNumber - 1) * pagingFilter.PageSize)
                                                .Take(pagingFilter.PageSize)
                                                .OrderByDescending(x => x.CreatedDate)
                                                .ToListAsync();
            return recruiterJobs;
         
        }
    }
}
