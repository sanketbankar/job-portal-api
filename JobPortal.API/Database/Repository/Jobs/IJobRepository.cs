﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository.Jobs
{
    public interface IJobRepository : IRepository<Job> 
    {
        Task<IEnumerable<Job>> GetJobs(PagingFilter pagingFilter);
        Task<IEnumerable<Job>> GetRecruiterPostedJobs(int userId, PagingFilter pagingFilter);
    }
}
