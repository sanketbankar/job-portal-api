﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;

namespace JobPortal.API.Database.Repository.Roles
{
    public class RoleRepository : Repository<Role> , IRoleRepository
    {
        public RoleRepository(JobPortalContext context) : base(context)
        {

        }
    }
}
