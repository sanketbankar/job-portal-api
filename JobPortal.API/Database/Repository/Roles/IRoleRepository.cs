﻿using JobPortal.API.Database.Repository.Repositories;
using JobPortal.API.Model;

namespace JobPortal.API.Database.Repository.Roles
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}
