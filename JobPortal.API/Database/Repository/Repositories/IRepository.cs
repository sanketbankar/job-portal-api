﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<T> Add(T entity);
        Task<List<T>>Add(List<T> entity);
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        Task<T> GetDefault(Expression<Func<T, bool>> expression);
        Task<T> GetById(int id);
        Task<IEnumerable<T>> Get();
        void Update(T entity);
        void Remove(T entity);
    }
}
