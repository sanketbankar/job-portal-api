﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JobPortal.API.Database.Repository.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected JobPortalContext JobPortalContext { get; set; }
        protected readonly JobPortalContext _context;
        public Repository(JobPortalContext context)
        {
            _context = context;
        }
        public virtual async Task<T> Add(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<List<T>> Add(List<T> entity)
        {
            await _context.AddRangeAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
           return  _context.Set<T>().Where(expression);
        }
        public async Task<T> GetDefault(Expression<Func<T, bool>> expression)
        {
            return await _context.Set<T>().Where(expression).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<T>> Get()
        {
            return await _context.Set<T>().ToListAsync();
        }
        public async Task<T> GetById(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }
        public void Remove(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChangesAsync();
        }
        public void Update(T entity)
        {
            _context.Set<T>().Update(entity);
            _context.SaveChangesAsync();
        }
    }
}
