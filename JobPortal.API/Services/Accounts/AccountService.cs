﻿using JobPortal.API.Model.DTOs;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Accounts
{
    public class AccountService : IAccountService
    {
        private readonly IConfiguration _configuration;
        public AccountService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<TokenDTO> Login (LoginModelDTO entity)
        {
           var claims = new List<Claim>
           {
               new Claim(ClaimTypes.Name, entity.UserName),
               new Claim("UserId", Convert.ToString(entity.UserId), ClaimValueTypes.Integer),
               new Claim("RoleId", Convert.ToString(entity.RoleId), ClaimValueTypes.Integer),
               new Claim(ClaimTypes.Role, entity.Role, ClaimValueTypes.String),
               new Claim("Email", entity.EmailId, ClaimValueTypes.String),
               new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
           };
           
           var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
           var token = new JwtSecurityToken(
           issuer:  _configuration["JWT:ValidIssuer"],
           audience: _configuration["JWT:ValidAudience"],
           claims: claims,
           expires: DateTime.Now.AddMinutes(30),
           signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256));
           
           
           TokenDTO token1 = new TokenDTO();
           token1.Token = new JwtSecurityTokenHandler().WriteToken(token);
           token1.ExpireTime = token.ValidTo;
            return token1;
        }
    }
}
