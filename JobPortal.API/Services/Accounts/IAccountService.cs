﻿using JobPortal.API.Model.DTOs;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Accounts
{
    public interface IAccountService
    {
        Task<TokenDTO> Login(LoginModelDTO entity);
    }
}
