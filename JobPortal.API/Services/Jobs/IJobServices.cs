﻿using JobPortal.API.Database;
using JobPortal.API.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Jobs
{
    public interface IJobServices
    {
        Task<IEnumerable<Job>> GetJobs(PagingFilter pagingFilter);
        Task<IEnumerable<Job>> GetRecruiterPostedJobs(int userId, PagingFilter pagingFilter);
        Task<Job> AddJob(Job model, int userId);
        Task<Job> UpdateJob(Job model);
        Task DeleteJob(int id);
    }
}
