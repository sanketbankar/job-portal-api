﻿using JobPortal.API.Database;
using JobPortal.API.Database.Repository.Jobs;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using JobPortal.API.Services.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Jobs
{
    public class JobServices : IJobServices
    {
        private readonly IJobRepository _jobRepository;
        private readonly JobPortalContext _context;
        public JobServices(IJobRepository jobRepository, JobPortalContext context)
        {
            _context = context;
            _jobRepository = jobRepository;
        }
        public async Task<IEnumerable<Job>> GetJobs(PagingFilter pagingFilter)
        {
            return await _jobRepository.GetJobs(pagingFilter);
        }
        public async Task<Job> AddJob(Job model, int userId)
        {
            var job = new Job();
            job.JobTitle = model.JobTitle;
            job.JobDescription = model.JobDescription;
            job.CreatedBy = userId;
            job.CreatedDate = DateTime.Now;
            job.IsActive = true;
            return await _jobRepository.Add(job);
        }

        public async Task DeleteJob(int id)
        {
            var job = await _jobRepository.GetById(id);
            if(job != null)
            {
                _jobRepository.Remove(job);
            }
        }

        public async Task<IEnumerable<Job>> GetRecruiterPostedJobs(int userId, PagingFilter pagingFilter)
        {
          return await _jobRepository.GetRecruiterPostedJobs(userId, pagingFilter);
        }

        public async Task<Job> UpdateJob(Job model)
        {
            try
            {
                var job = await _jobRepository.GetById(model.Id);
                if (job != null)
                {
                    job.JobTitle = model.JobTitle;
                    job.JobDescription = model.JobDescription;
                   // job.CreatedBy = model.CreatedBy;
                   // job.CreatedDate = model.CreatedDate;
                    job.IsActive = model.IsActive;
                    _jobRepository.Update(job);
                    _context.SaveChanges();
                    return job;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return null;
        }
    }
}
