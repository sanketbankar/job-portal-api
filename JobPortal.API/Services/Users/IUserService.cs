﻿using JobPortal.API.Database;
using JobPortal.API.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Users
{
    public interface IUserService
    {
        Task<User> Authenticate(string emailId, string password);
        Task<bool> CheckEmailExists(string emailId);
        Task<User> FindByName(string name);
        Task<User> AddCandidateInfo(User model);
        Task<IEnumerable<User>> GetAllUsers(PagingFilter pagingFilter);
        Task<IEnumerable<User>> GetUser();
        Task<IEnumerable<User>> GetCandidates();
        Task<IEnumerable<User>> GetRecruiters();
        Task<IEnumerable<User>> GetAdmin();
        Task<IEnumerable<User>> AddUser(List<User> model);
        Task<User> Update(User model);
        Task<bool> Delete(int id);
        Task<bool> ForgotPassword(string email);
        Task<User> ResetPassword(int otp, string newPassword, string confirmPassword);
    }
}
