﻿using JobPortal.API.Database;
using JobPortal.API.Database.Repository;
using JobPortal.API.Model;
using JobPortal.API.Services.Emails;
using JobPortal.API.Services.JobAlert;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IJobOtpAlertService _jobOtpAlertService;
        private readonly IMailService _mailService;
        public UserService(IUserRepository userRepository, IJobOtpAlertService jobOtpAlertService, IMailService mailService)
        {
            _userRepository = userRepository;
            _jobOtpAlertService = jobOtpAlertService;
            _mailService = mailService;
        }

        public async Task<User> Authenticate(string emailId, string password)
        {
            try
            {
                return await _userRepository.GetDefault(x => x.EmailId == emailId && x.Password == password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<User>> GetCandidates()
        {
            return await _userRepository.GetCandidates();
        }

        public async Task<IEnumerable<User>> GetRecruiters()
        {
            return await _userRepository.GetRecruiters();
        }

        public async Task<User> AddCandidateInfo(User model)
        {
            try
            {
                var user = new User();
                user.Name = model.Name;
                user.EmailId = model.EmailId;
                user.Password = model.Password;
                user.RoleId = 1;
                user.IsActive = true;
                await _userRepository.Add(user);
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> CheckEmailExists(string emailId)
        {
            var user = await _userRepository.GetDefault(x => x.EmailId == emailId);
            if (user != null)

                return true;
            return false;
        }
        public async Task<User> FindByName(string name)
        {
            var user = await _userRepository.GetDefault(x => x.Name == name);
            if (user != null)

                return user;
            return null;
        }
        public async Task<User> FindByEmail(string email)
        {
            var user = await _userRepository.GetDefault(x => x.EmailId == email);
            if (user != null)

                return user;

            return null;
        }
        public async Task<IEnumerable<User>> AddUser(List<User> model)
        {
            IEnumerable<User> user = await _userRepository.Add(model);
            return user;
        }
        public async Task<bool> Delete(int id)
        {
            var job = await _userRepository.GetById(id);
            if (job != null)
            {
                _userRepository.Remove(job);
                return true;
            }
            return false;
        }
        public async Task<IEnumerable<User>> GetAllUsers(PagingFilter pagingFilter)
        {
            var user = await _userRepository.GetAllUsers(pagingFilter);
            return user;
        }
        public async Task<IEnumerable<User>> GetUser()
        {
            var user = await _userRepository.GetUser();
            return user;
        }

        public async Task<User> Update(User model)
        {
            try
            {
                var user = await _userRepository.GetById(model.Id);
                if (user != null)
                {
                    user.Name = model.Name;
                    user.EmailId = model.EmailId;
                    user.Password = model.Password;
                    _userRepository.Update(user);
                    return user;
                }
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<User>> GetAdmin()
        {
            return await _userRepository.GetAdmin();
        }

        public async Task<bool> ForgotPassword(string email)
        {
            try
            {
                var user = await FindByEmail(email);
                if (user != null)
                {
                    var otp = RandomNumberGenerator.Generate(100000, 999999);
                    var to = user.EmailId;
                    var sub = "Password Reset";
                    var body = "";
                    body += $"<h5> Hi, {user.Name}</h5>";
                    body += $"<h4> OTP for Job Portal : {otp}</h4>";
                    body += "<h5> Note : OTP is valid for 10 minutes </h5>";

                    var userOtp = new JobOtpAlert();
                    userOtp.Otp = Convert.ToInt32(otp);
                    userOtp.UserId = user.Id;
                    userOtp.ExpireTime = DateTime.Now.AddMinutes(10);

                    await _jobOtpAlertService.AddOtp(userOtp);
                    await _mailService.SendEmail(to, sub, body);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<User> ResetPassword(int otp, string newPassword, string confirmPassword)
        {
            try
            {
                var details = await _jobOtpAlertService.ValidateOtp(otp);
                var updateUser = new User();
                if (details != null)
                {
                    updateUser = await _userRepository.GetById(details.UserId);
                    if (newPassword == confirmPassword)
                    {
                        updateUser.Password = newPassword;
                        _userRepository.Update(updateUser);
                        var to = updateUser.EmailId;
                        var subject = "Password Updated";
                        var message = "Your password has been updated successfully.";
                        await _mailService.SendEmail(to, subject, message);
                        return updateUser;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
