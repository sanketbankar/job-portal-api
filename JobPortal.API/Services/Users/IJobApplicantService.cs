﻿using JobPortal.API.Database;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Users
{
    public interface IJobApplicantService
    {
        Task<JobApplicant> CandidatesApply(JobApplicant entity, int userId);
        Task<IEnumerable<JobApplicant>> Apply(List<JobApplicant> entities);
        Task<IEnumerable<RecruiterJobsDTO>> GetCandidateAppliedToRecruitersJob(int userId, PagingFilter pagingFilter);
        Task<IEnumerable<CandidateDetailsDTO>> GetAllCandidatesJobsApplied(PagingFilter pagingFilter);
        Task<IEnumerable<CandidateDetailsDTO>> GetCandidatesAllJobsApplied(int userId, PagingFilter pagingFilter);
    }
}
