﻿using JobPortal.API.Database;
using JobPortal.API.Database.Repository;
using JobPortal.API.Database.Repository.Jobs;
using JobPortal.API.Database.Repository.Users;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using JobPortal.API.Services.Emails;
using JobPortal.API.Services.JobAlert;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Users
{
    public class JobApplicantService : IJobApplicantService
    {
        private readonly IJobApplicantRepository _jobApplicantRepository;
        private readonly IUserRepository _userRepository;
        private readonly IJobRepository _jobRepository;
        private readonly IMailService _mailService;
        private readonly IJobOtpAlertService _jobOtpAlertService;
        public JobApplicantService(IJobApplicantRepository jobApplicantRepository,
                                   IUserRepository userRepository,
                                   IJobRepository jobRepository,
                                   IMailService mailService,
                                   IJobOtpAlertService jobOtpAlertService)
        {
            _jobApplicantRepository = jobApplicantRepository;
            _userRepository = userRepository;
            _jobRepository = jobRepository;
            _mailService = mailService;
            _jobOtpAlertService = jobOtpAlertService;
        }
        public async Task<JobApplicant> CandidatesApply(JobApplicant entity, int userId)
        {
            try
            {
                var job = new JobApplicant();
                job.JobId = entity.JobId;
                job.AppliedBy = userId;
                job.AppliedDate = DateTime.Now;
                job.IsActive = true;
                var userDetails = await _userRepository.GetById(userId);
                var jobDetails = await _jobRepository.GetById(job.JobId);
                if (jobDetails != null)
                {
                    await _jobApplicantRepository.Add(job);
                    var to = userDetails.EmailId;
                    var subject = "Job Applied";
                    var body = "You have successfully applied for the job " + jobDetails.JobTitle;
                    await _mailService.SendEmail(to, subject, body);

                    var recruiter = await _userRepository.GetById(jobDetails.CreatedBy);
                    var subjectRecruiter = "Job Applied";
                    var bodyRecruiter = $"One Candidate have applied for the job you have posted. <br/> Candidate EmailId : {userDetails.EmailId} <br/> Candidate Name : {userDetails.Name} <br/> Job Title : {jobDetails.JobTitle}";
                    await _mailService.SendEmail(recruiter.EmailId, subjectRecruiter, bodyRecruiter);
                    return job;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<JobApplicant>> Apply(List<JobApplicant> entities)
        {
            IEnumerable<JobApplicant> application = await _jobApplicantRepository.Add(entities);
            return application;
        }
        public async Task<IEnumerable<RecruiterJobsDTO>> GetCandidateAppliedToRecruitersJob(int userId, PagingFilter pagingFilter)
        {
            return await _jobApplicantRepository.GetCandidateAppliedToRecruitersJob(userId, pagingFilter);
        }
        public async Task<IEnumerable<CandidateDetailsDTO>> GetAllCandidatesJobsApplied(PagingFilter pagingFilter)
        {
            return await _jobApplicantRepository.GetAllCandidatesJobsApplied(pagingFilter);
        }
        public async Task<IEnumerable<CandidateDetailsDTO>> GetCandidatesAllJobsApplied(int userId, PagingFilter pagingFilter)
        {
            return await _jobApplicantRepository.GetCandidatesAllJobsApplied(userId, pagingFilter);
        }
    }
}
