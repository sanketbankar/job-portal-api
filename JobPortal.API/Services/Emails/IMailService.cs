﻿using JobPortal.API.Model;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Emails
{
    public interface IMailService
    {
        Task SendEmail(string to, string subject, string message);
        Task SendNotification(Mail model);
    }
}
