﻿using JobPortal.API.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Emails
{
    public class MailService : IMailService
    {
        private readonly IConfiguration _configuration;
        public MailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Task SendEmail(string to, string subject, string message)
        {
            Execute(to, subject, message).Wait();
            return Task.FromResult(0);
        }
        public async Task Execute(string to, string subject, string message)
        {
            try
            {
                var mail = new MailMessage()
                {
                    From = new MailAddress(_configuration["MailSettings:Mail"], _configuration["MailSettings:DisplayName"])
                };
                to.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(t => mail.To.Add(new MailAddress(t)));
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                using (SmtpClient smtp = new SmtpClient(_configuration["MailSettings:Host"], Convert.ToInt32(_configuration["MailSettings:Port"])))
                {
                    smtp.Credentials = new NetworkCredential(_configuration["MailSettings:Mail"], _configuration["MailSettings:Password"]);
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(mail);
                }
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }
        public Task SendNotification(Mail model)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Hi,");
            sb.Append($"<br /><br /><br />{model.UserId} Have Successfully Applied for a Job {model.JobTitle}.");
            model.Message = sb.ToString();
            Execute(model.To, model.Subject, model.Message).Wait();
            return Task.FromResult(0);
        }
    }
}

