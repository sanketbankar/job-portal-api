﻿using JobPortal.API.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Roles
{
    public interface IRoleService
    {
        Task<IEnumerable<Role>> GetRoles();
        Task<Role> GetRolesById(int id);
        Task AddRoles(Role model);
        Task<Role> UpdateRole(Role model);
        Task<bool> DeleteRole(int id);
    }
}
