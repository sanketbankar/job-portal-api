﻿using JobPortal.API.Database.Repository.Roles;
using JobPortal.API.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Services.Roles
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task AddRoles(Role model)
        {
            await _roleRepository.Add(model);
        }

        public async Task<bool> DeleteRole(int id)
        {
            var role = await _roleRepository.GetById(id);
            if (role != null)
            {
                _roleRepository.Remove(role);
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<Role>> GetRoles()
        {
            return await _roleRepository.Get();
        }

        public async Task<Role> GetRolesById(int id)
        {
            return await _roleRepository.GetById(id);
        }

        public async Task<Role> UpdateRole(Role model)
        {
            var role = await _roleRepository.GetById(model.Id);
            if (role != null)
            {
                role.Name = model.Name;
                _roleRepository.Update(role);
                return role;
            }
            return role;
        }
    }
}
