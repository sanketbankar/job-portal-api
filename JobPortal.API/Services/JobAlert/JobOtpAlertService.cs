﻿using JobPortal.API.Database.Repository.JobAlert;
using JobPortal.API.Model;
using System;
using System.Threading.Tasks;

namespace JobPortal.API.Services.JobAlert
{
    public class JobOtpAlertService : IJobOtpAlertService
    {
        private readonly IJobOtpAlertRepository _jobOtpAlertRepository;
        public JobOtpAlertService(IJobOtpAlertRepository jobOtpAlertRepository)
        {
            _jobOtpAlertRepository = jobOtpAlertRepository;
        }
        public Task<JobOtpAlert> AddOtp(JobOtpAlert model)
        {
            return _jobOtpAlertRepository.Add(model);
        }

        public async Task<JobOtpAlert> ValidateOtp(int otp)
        {
            return await _jobOtpAlertRepository.GetDefault(x => x.Otp == otp && x.ExpireTime >= DateTime.Now);
        }

        public string GenerateOtp(User user)
        {
            string numbers = "0123456789";
            Random random = new Random();
            var otp = string.Empty;
            for (int i = 0; i < 5; i++)
            {
                int tempval = random.Next(0, numbers.Length);
                otp += tempval;
            }
            return otp;
        }
    }
}
