﻿using JobPortal.API.Model;
using System.Threading.Tasks;

namespace JobPortal.API.Services.JobAlert
{
    public interface IJobOtpAlertService
    {
        Task<JobOtpAlert> AddOtp(JobOtpAlert model);
        Task<JobOtpAlert> ValidateOtp(int num);
        string GenerateOtp(User user);
    }
}
