﻿using JobPortal.API.Database;
using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using JobPortal.API.Services.Emails;
using JobPortal.API.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Controllers
{
    
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    [ApiController]
    public class JobApplicantController : BaseController
    {
        private readonly IJobApplicantService _jobApplicantService;
        public JobApplicantController(IJobApplicantService jobApplicantService)
        {
            _jobApplicantService = jobApplicantService;
        }
        
        [HttpPut]
        [Route("JobApply")]
        [Authorize(Policy = "Candidate")]
        public async Task<IActionResult> CandidateApply(JobApplicant entity)
        {
            var application = await _jobApplicantService.CandidatesApply(entity, UserId);
            if (application !=null )
            {
                return Ok(application);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("Candidate/Applied/RecruiterJob")]
        [Authorize(Policy = "RecruiterandAdmin")]
        public async Task<IEnumerable<RecruiterJobsDTO>> GetCandidateAppliedToRecruitersJob([FromQuery] PagingFilter pagingFilter)
        {
            return await _jobApplicantService.GetCandidateAppliedToRecruitersJob(UserId, pagingFilter);
        }

        [HttpGet]
        [Route("AllCandidatesJobsApplied")]
        [Authorize(Policy = "Admin")]
        public async Task<IEnumerable<CandidateDetailsDTO>> GetAllCandidatesJobsApplied([FromQuery] PagingFilter pagingFilter)
        {
            return await _jobApplicantService.GetAllCandidatesJobsApplied(pagingFilter);
        }

        [HttpGet]
        [Route("CandidatesAllJobsApplied")]
        [Authorize(Policy = "Candidate")]
        public async Task<IEnumerable<CandidateDetailsDTO>> GetCandidatesAllJobsApplied([FromQuery] PagingFilter pagingFilter)
        {
            return await _jobApplicantService.GetCandidatesAllJobsApplied(UserId, pagingFilter);
        }
    }
}
