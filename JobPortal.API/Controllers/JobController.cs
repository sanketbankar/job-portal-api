﻿using JobPortal.API.Database;
using JobPortal.API.Model;
using JobPortal.API.Services.Jobs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : BaseController
    {
        private readonly IJobServices _jobServices;
        public JobController(IJobServices jobServices)
        {
            _jobServices = jobServices;
        }

        [HttpGet]
        [Route("Jobs")]
        [Authorize(Policy = "Admin")]
        public async Task<IEnumerable<Job>> GetJobs([FromQuery] PagingFilter pagingFilter)
        {
            return await _jobServices.GetJobs(pagingFilter);
        }

        [HttpGet]
        [Route("RecruiterPostedJobs")]
        [Authorize(Policy = "RecruiterandAdmin")]
        public async Task<IActionResult> GetRecruiterPostedJobs([FromQuery] PagingFilter pagingFilter)
        {
            var jobs = await _jobServices.GetRecruiterPostedJobs(UserId, pagingFilter);
            if (jobs != null)
            {
                return Ok(jobs);
            }
            return NotFound();
        }

        [HttpPost]
        [Route("Jobs")]
        [Authorize(Policy = "RecruiterandAdmin")]
        public async Task<IActionResult> AddJob (Job model)
        {
            if(ModelState.IsValid)
            {
               var job = await _jobServices.AddJob(model, UserId);
                return Ok(job);
            }
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Route("Job")]
        [Authorize(Policy = "RecruiterandAdmin")]
        public async Task<IActionResult> UpdateJobs(Job model)
        {
            var updateJob = await _jobServices.UpdateJob(model);
            if (updateJob != null)
            {
                return Ok(new APIResponse { Code = StatusCodes.Status200OK, Message = "Job Details updated succesfully..." });
            }
            return BadRequest();
        }

        [HttpDelete]
        [Route("Jobs/{Id}")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> DeleteJobs(int Id)
        {
            await _jobServices.DeleteJob(Id);
            return Ok();
        }
    }
}
