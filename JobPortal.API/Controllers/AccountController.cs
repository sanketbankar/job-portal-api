﻿using JobPortal.API.Model;
using JobPortal.API.Model.DTOs;
using JobPortal.API.Services.Accounts;
using JobPortal.API.Services.Emails;
using JobPortal.API.Services.Roles;
using JobPortal.API.Services.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace JobPortal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IMailService _mailService;
        private readonly IAccountService _accountService;
        public AccountController(
                                 IUserService userService,
                                 IRoleService roleService,
                                 IMailService mailService,
                                 IAccountService accountService)
        {
            _userService = userService;
            _roleService = roleService;
            _mailService = mailService;
            _accountService = accountService;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(User model)
        {
            var existingUser = await _userService.CheckEmailExists(model.EmailId);
            var user = await _userService.GetUser();
            if (existingUser == true)
            {
                return BadRequest(new APIResponse { Status = "Existing User", Message = "User Already Exists" });
            }
            else
            {
                var to = model.EmailId;
                var subject = "Successfully Registered";
                var message = $"{model.Name} Welcome to Job Portal";
                await _mailService.SendEmail(to, subject, message);
                var result = _userService.AddCandidateInfo(model);
                return Ok(new APIResponse { Status = "Success", Message = "Registration Successfully done...", Data = result });
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(string emailId, string password)
        {
            var user = await _userService.Authenticate(emailId, password);
            if (user != null)
            {
                var role = await _roleService.GetRolesById(user.RoleId);
                LoginModelDTO login = new LoginModelDTO();
                login.UserId = user.Id;
                login.UserName = user.Name;
                login.EmailId = user.EmailId;
                login.Password = user.Password;
                login.RoleId = user.RoleId;
                login.Role = role.Name;

                // var to = user.EmailId;
                // var subject = "Successfully LoggedIn";
                // var message = $" Welcome to Job Portal";
                // await _mailService.SendEmailAsync(to, subject, message);

                return Ok(await _accountService.Login(login));
            }
            return BadRequest("Invalid EmailId or Password");
        }

        [HttpPost]
        [Route("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            var user = await _userService.ForgotPassword(email);
            if (user == true)
            {
                return Ok(new APIResponse { Code = StatusCodes.Status200OK, Message = "Otp sent to the register email address..." });
            }
            return NotFound(new APIResponse { Code = StatusCodes.Status400BadRequest, Message = "Incorrect Details..." });
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword(int otp, string newPassword, string confirmPassword)
        {
            var user = await _userService.ResetPassword(otp, newPassword, confirmPassword);
            if (user != null)
            {
                return Ok(new APIResponse { Code = StatusCodes.Status200OK, Message = "Your Password has been updated succesfully..." });
            }
            return NotFound(new APIResponse { Code = StatusCodes.Status400BadRequest, Message = "Incorrect Details.." });
        }
    }
}
