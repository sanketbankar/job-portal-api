﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobPortal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        public BaseController()
        {

        }
        protected int UserId => int.Parse(this.User.Claims.First(i => i.Type == "UserId").Value);
        protected int RoleId => int.Parse(this.User.Claims.First(i => i.Type == "RoleId").Value);
    }
}
