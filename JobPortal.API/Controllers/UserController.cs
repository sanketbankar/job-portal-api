﻿using JobPortal.API.Database;
using JobPortal.API.Model;
using JobPortal.API.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("Users")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> GetAllUsers([FromQuery]PagingFilter pagingFilter)
        {
            var users = await _userService.GetAllUsers(pagingFilter);
            if (users !=null)
            {
                return Ok(users);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("Candidates")]
        [Authorize(Policy = "RecruiterandAdmin")]
        public async Task<IActionResult> GetCandidates()
        {
           var candidates = await _userService.GetCandidates();
            if(candidates != null)
            {
                return Ok(candidates);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("Recruiters")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> GetRecruiters()
        {
            var recruiter = await _userService.GetRecruiters();
            if (recruiter != null)
            {
                return Ok(recruiter);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("Admin")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> GetAdmin()
        {
            var admin = await _userService.GetAdmin();
            if (admin != null)
            {
                return Ok(admin);
            }
            return NotFound();
        }

        [HttpDelete]
        [Route("User/{id}")]
        [Authorize(Policy = "Admin")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            await _userService.Delete(id);
            return Ok();
        }
    }
}
