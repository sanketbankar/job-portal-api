﻿using JobPortal.API.Model;
using JobPortal.API.Services.Roles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.API.Controllers
{
    [Authorize(Policy = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        [Route("Role")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _roleService.GetRoles());
        }

        [HttpGet]
        [Route("Role/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await _roleService.GetRolesById(id));
        }

        [HttpPost]
        [Route("Role")]
        public async Task<IActionResult> Add(Role entity)
        {
            return Ok( _roleService.AddRoles(entity));
        }

        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(Role entity)
        {
            return Ok(await _roleService.UpdateRole(entity));
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete(int Id)
        {
            return Ok( await _roleService.DeleteRole(Id));
        }
    }
}
